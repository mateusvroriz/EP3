class Message < ActiveRecord::Base
	validates :description, presence: true, allow_blank: false
	belongs_to :user
	has_many :comments
end
